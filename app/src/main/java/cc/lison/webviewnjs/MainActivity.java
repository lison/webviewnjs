package cc.lison.webviewnjs;

import android.app.Activity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.net.HttpRetryException;

public class MainActivity extends AppCompatActivity {

    EditText et;
    Button btn;
    WebView wv;
    WebSettings ws;

    Activity activity;
    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        activity = this;
        actionBar = getSupportActionBar();

        et = (EditText) findViewById(R.id.et);
        btn = (Button) findViewById(R.id.btn);
        wv = (WebView) findViewById(R.id.webview);

        //android调用WebView的js方法
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wv.loadUrl("javascript:fill(" + "'" + et.getText().toString() + "'" + ")");
            }
        });

        wv.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                activity.setProgress(progress * 1000);
            }
        });

        wv.loadUrl("http://192.168.1.200:8000/index.html");

        //注册本地方法供js调用，并暴露对象名为ntv，js使用window.ntv.functionName()方式调用
        wv.addJavascriptInterface(new JsCall(), "ntv");

        ws = wv.getSettings();
        ws.setJavaScriptEnabled(true);

        //不使用缓存
        ws.setCacheMode(WebSettings.LOAD_NO_CACHE);
        //支持缩放
        ws.setSupportZoom(true);
        //自适应屏幕
        ws.setUseWideViewPort(true);
        ws.setLoadWithOverviewMode(true);
    }

    //js调用的类及方法
    class JsCall {
        @JavascriptInterface
        public String tost(String msg) {
            Toast.makeText(activity, "toast: " + msg, Toast.LENGTH_SHORT).show();
            return "JsCalled Native: " + msg;
        }
    }

    //回退栈（WebView页面）
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (wv.canGoBack()) {
                wv.goBack();//返回上一浏览页面
                return true;
            } else {
                finish();//关闭Activity
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    //ActionBar菜单
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //ActionBar菜单响应
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item:
                Toast.makeText(this, "Menu Item", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
